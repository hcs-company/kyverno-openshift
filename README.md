# Example Multi-Tenant Kyverno Policies for OpenShift

## Prerequisites

These policies assume you have your cluster access and groups set up as the following:

- You have `kyverno` installed on your cluster
- Your (synced) groups are named using the `<teamname>-<subteam>` pattern.
  For example, `foobar-dev`, `foobar-adm`, etc.
- Your namespaces are prefixed with the same prefix as the groups they belong to,
  for example: `foobar-cicd`, `foobar-production`, etc.
- You would like your teams to be able to self-provision their projects/namespaces.
  This means that you have **not** revoked the `self-provisioner` role from
  your regular users, or you have added this role to a select group of
  users/teams
- Your teams each have a `<teamname>-cicd` namespace in which their major
  tooling (pipelines with Tekton, ArgoCD, etc.) runs. Some of the
  ServiceAccounts in use by these tools have access to the `self-provisioner`
  role and/or enough privileges to create Namespace objects.

## Goals

- Regular users can create Projects (not namespaces) as long as the project
  name has a prefix that either matches their username, or the team prefix of
  one of their groups.
- Privileged ServiceAccounts can create new Projects **and** Namespaces as long
  as the new project/namespace name matches the prefix of the namespace the
  ServiceAccount belongs to, **and** the ServiceAccount is from a namespace
  that has the `-cicd` postfix.
- New Projects are labeled with argocd.argoproj.io/managed-by: <gitops-instance namespace> 
  so that new Projects automatically can be managed by an application ArgoCD instance
- kube:admin & system:admin are excluded from the restrict-namespace-names policy. 
  (Stretched goal: exclude all users with a : in their name)

## Extra Goals

- We want to use ClusterResourceQuotas using a label selector on all namespaces.
  In order to achieve this newly created namespaces should automatically get the
  label `quotagroup` with the value of the team prefix. For example, the
  namespace `foobar-acceptance` should get the label `quotagroup: foobar`.
- A regular user can rollout a Service Mesh controlplane by creating a *-servicemesh
  namespace. A servicemeshmemberroll rolebinding is created for `*-OPS` LDAP group.
  The LDAP group may contain an environment specific part. That part is specified by a
  config map.

## Policies

The policy `policies/restrict-namespace-names.yaml` ensures that new Namespaces
and Project(Request)s meet the criteria listed under [Goals](#goals)

The policy `policies/label-namespaces.yaml` ensures that new Namespaces meet
the goals listed under [Extra Goals](#extra-goals)

The policy `policies/add-servicemesh-to-namespace.yaml` ensures that new Namespaces meet
the goals listed under [Extra Goals](#extra-goals)

## Add ServiceMesh to namespace policy
This policy deploys a ServiceMesh control plane in a *-servicemesh namespace (like foobar-servicemesh) without a user
requiring additional privileges. A group is granted the servicemeshmemberroll to manage which namespaces are managed by the servicemesh instance. Sometimes groups have environment or cluster specific names. This policy contains an example where the environment specific part is fetched from a configmap. The configmap can be placed in any namespace under your controle like foobar-kyverno.

```
  - name: servicemesh-rolebinding
    context:
    - name: ldapdomain
      configMap:
        name: kyverno-parameters
        namespace: foobar-kyverno
    ...
    generate:
      synchronize: false
      apiVersion: rbac.authorization.k8s.io/v1
      kind: RoleBinding
      name: "{{ request.object.metadata.name }}-servicemeshmemberroll"
      namespace: "{{ request.object.metadata.name }}"
      data:
        subjects:
        - kind: Group
          name: "{{ ldapdomain.data.ldapdomain }}-{{ to_upper('{{ namespaceprefix }}') }}-OPS"
```